package com.uncleYar.paswdGen;

import java.security.NoSuchAlgorithmException;
import javax.crypto.KeyGenerator;

public class GenerateKey {
	public static final String AES = "AES";
	
    private static String byteArrayToHexString(byte[] b) {
        StringBuffer sb = new StringBuffer(b.length * 2);
        for (int i = 0; i < b.length; i++) {
            int v = b[i] & 0xff;
            if (v < 16) {
                sb.append('0');
            }
            sb.append(Integer.toHexString(v));
        }
        return sb.toString().toUpperCase();
    }
    
    public static String getKey() throws NoSuchAlgorithmException {
    	KeyGenerator keyGen = KeyGenerator.getInstance(GenerateKey.AES);
        keyGen.init(128);
        return byteArrayToHexString(keyGen.generateKey().getEncoded());
    }
}
